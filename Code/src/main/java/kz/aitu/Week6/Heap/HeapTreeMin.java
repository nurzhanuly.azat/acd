package kz.aitu.Week6.Heap;

public class HeapTreeMin {
    private Node root;

    public void insertNode (int data){
        if( root == null ) {
            root = new Node(data);
            return;
        }
        if(root.getData() < data){
            int temp = root.getData();
            root.setData(data);
            traverse(root,temp);
        } else {
            traverse(root,data);
        }
    }

    private void traverse (Node temp, int data) {
        Node node = temp;
        if(node == null) {
            node = new Node(data);
            return;
        }
        if(data > node.getData()){
            if(node.getRight() == null){
                node.setRight(new Node(data));
                return;
            }
            traverse(node.getRight(),data);
        }
        if(data <= node.getData()){
            if(node.getLeft() == null) {
                node.setLeft(new Node(data));
                return;
            }
            int t = node.getData();
            node.setData(data);
            data = t;
            traverse(node.getRight(),data);
        }
    }

    public void print(Node node){
        if(node == null) return;
        Node temp = node;
        print(temp.getLeft());
        System.out.print(temp.getData());
        print(temp.getRight());
    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }
}

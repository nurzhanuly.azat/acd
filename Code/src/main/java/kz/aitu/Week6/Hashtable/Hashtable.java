package kz.aitu.Week6.Hashtable;

public class Hashtable {
    private int tablesize;
    private Node[] hashtable;
    double loadfactor = 0;

    public Hashtable(int tablesize) {
        this.tablesize = tablesize;
        hashtable = new Node[tablesize];
    }

    public void addValue(String s1, String s2) {
        Node node = new Node(s1,s2);
        int index = s1.hashCode()%tablesize;

        if(hashtable[index] == null) {
            hashtable[index] = node;
        } else {
            Node temp = hashtable[index];
            while (temp.getNext() != null) {
                temp = temp.getNext();
            }
            temp.setNext(node);
        }
    }

    public void set (String key, String value) {
        int index = Math.abs(key.hashCode()%tablesize);
        if(hashtable[index] == null){
            hashtable[index] = new Node(key, value);
            return;
        }
        Node temp = hashtable[index];

        while(temp != null){
            if(temp.getKey() == key){
                temp.setValue(value);
                break;
            }
            temp = temp.getNext();
        }
        temp = new Node(key,value);
    }

    public Node get (String key) {
        int index = key.hashCode()%tablesize;
        Node temp = hashtable[index];

        while(temp != null){
            if(temp.getKey() == key) return temp;
            temp = temp.getNext();
        }
        return null;
    }

    public void removeNode (String key) {
        Node temp = hashtable[key.hashCode()%tablesize];
        int index = key.hashCode()%tablesize;

        if(temp == null) return;

        if(temp.getKey() == key){
            if(temp.getNext() == null) {
                hashtable[index] = null;
            } else {
                hashtable[index] = temp.getNext();
            }
        }
        else{
            while (temp != null){
                if(key == temp.getNext().getKey()){
                   temp = temp.getNext().getNext();
                   break;
                }
                temp = temp.getNext();
            }
        }
    }

    public void printIndexOfHashTable (int index) {
        Node temp = hashtable[index];
        while (temp != null) {
            System.out.println(temp.getValue());
            temp = temp.getNext();
        }
    }

    public void printAllTable () {
        for(int i = 0; i < tablesize; i++){
            Node temp = hashtable[i];
            System.out.print("values at index " + i + ":");
            while(temp != null){
                System.out.print(" " + temp.getValue());
                temp = temp.getNext();
            }
            System.out.println();
        }
    }

    public String findValue (String key) {
        Node temp = hashtable[key.hashCode()%3];
        while (key != temp.getKey()){
            temp = temp.getNext();
        }
        return temp.getValue();
    }

    public int size (Hashtable hashtable) {
        int counter = 0;
        for(int i = 0; i < hashtable.getTablesize();i++){
            Node temp = hashtable.getHashtable()[i];
            while(temp != null){
                temp = temp.getNext();
                counter++;
            }
        }
        return counter;
    }

    public double getLoadfactor() {
        return loadfactor;
    }

    public void setLoadfactor(double loadfactor) {
        this.loadfactor = loadfactor;
    }

    public int getTablesize() {
        return tablesize;
    }

    public void setTablesize(int tablesize) {
        this.tablesize = tablesize;
    }

    public Node[] getHashtable() {
        return hashtable;
    }

    public void setHashtable(Node[] hashtable) {
        this.hashtable = hashtable;
    }
}

package kz.aitu.StackImplementation;

public class Stack {
    private Node top;

    public void push(int data) {
        Node node = new Node(data);
        if(top == null){
            top = node;
        } else {
            node.setNext(top);
            top = node;
        }
    }

    public void pop () {
        if (top.getNext() != null) top = top.getNext();
        else
            top = null;
    }

}

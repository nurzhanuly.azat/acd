package kz.aitu.week9_graph;

public class Vertex {
    private int key;
    private String value;
    private LinkedList edges;
    private Vertex next;
    private Vertex nextforlist;

    public void addEdge (Vertex v) {
        if (!(edges.contain(v))) edges.addEdge(v);
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public LinkedList getEdges() {
        return edges;
    }

    public void setEdges(LinkedList edges) {
        this.edges = edges;
    }

    public Vertex getNext() {
        return next;
    }

    public void setNext(Vertex next) {
        this.next = next;
    }

    public Vertex getNextforlist() {
        return nextforlist;
    }

    public void setNextforlist(Vertex nextforlist) {
        this.nextforlist = nextforlist;
    }

    public Vertex(int key, String value) {
        this.key = key;
        this.value = value;
        this.edges = new LinkedList();
    }
}

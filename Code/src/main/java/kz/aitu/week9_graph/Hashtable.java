package kz.aitu.week9_graph;

public class Hashtable {
    private int size = 11;
    private Vertex[] hashtable;

    public Hashtable() {
        hashtable = new Vertex[size];
    }

    public boolean isKey(int key) {
        int index = key % size;
        if (hashtable[index] == null) return false;
        Vertex temp = hashtable[index];
        while (temp != null) {
            if (temp.getKey() == key) return true;
            temp = temp.getNext();
        }
        return false;
    }

    public void addVertex(int key, String value) {
        int index = key%size;
        Vertex v = new Vertex(key,value);
        if (hashtable[index] == null) {
            hashtable[index] = v;
        } else {
            v.setNext(hashtable[index]);
            hashtable[index] = v;
        }
    }

    public Vertex getVertex(int key) {
        int index = key%size;
        if(hashtable[index] == null) return null;
        Vertex temp = hashtable[index];
        while (temp != null) {
            if(key == temp.getKey()) return temp;
            temp = temp.getNext();
        }
        return null;
    }


    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Vertex[] getHashtable() {
        return hashtable;
    }

    public void setHashtable(Vertex[] hashtable) {
        this.hashtable = hashtable;
    }
}

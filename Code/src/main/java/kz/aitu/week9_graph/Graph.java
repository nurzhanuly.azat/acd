package kz.aitu.week9_graph;

public class Graph {
    private boolean biDirectional;
    Hashtable hashtable = new Hashtable();

    public Graph(boolean biDirectional) {
        this.biDirectional = biDirectional;
    }

    public void addVertex (int key, String value) {
        Vertex v = new Vertex(key,value);

        if (!(hashtable.isKey(key))) hashtable.addVertex(key, value);
    }

    public void addEdge(int key, int key1, boolean biDirectional) {
        Vertex v = hashtable.getVertex(key);
        Vertex v1 = hashtable.getVertex(key);
        if (biDirectional) {
            v.addEdge(v1);
            v1.addEdge(v);
        } else {
            v.addEdge(v1);
        }
    }

    public boolean isBiDirectional() {
        return biDirectional;
    }

    public void setBiDirectional(boolean biDirectional) {
        this.biDirectional = biDirectional;
    }

    public Hashtable getHashtable() {
        return hashtable;
    }

    public void setHashtable(Hashtable hashtable) {
        this.hashtable = hashtable;
    }
}

package kz.aitu.gradeUp;

public class LinkedList {
    private Node head;
    private Node tail;

    public void addNode(int data) {
        Node node = new Node(data);
        if (head == null) {
            head = node;
            tail = node;
        } else {
            tail.setNext(node);
            tail = node;
        }
    }

    public void deleteNodeHead() {
        head = head.getNext();
    }

    public void addAtHead(int data) {
        Node node = new Node(data);
        node.setNext(head);
        head = node;
    }

    public void addAtIndext(Node node, int data, int index) {
        Node tmp = new Node(data);
        Node n = node;
        for (int i = 0; i < index - 1; i++) {
            n = n.getNext();
        }
        tmp.setNext(n.getNext());
        n.setNext(tmp);
    }

    public void deleteAtIndex(Node node, int index) {
        Node temp = node;

        if (index == 0) {
            deleteNodeHead();
        }

        for (int i = 0; i < index - 1; i++) {
            if (temp.getNext() == null) return;
            temp = temp.getNext();
        }
        temp.setNext(temp.getNext().getNext());
    }

    public void print() {
        Node t = head;
        while (t != null) {
            System.out.println(t.getData());
            t = t.getNext();
        }
    }


    public Node getHead() {
        return head;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public Node getTail() {
        return tail;
    }

    public void setTail(Node tail) {
        this.tail = tail;
    }
}

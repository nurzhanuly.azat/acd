package kz.aitu.week1.homework;

import java.util.Scanner;

public class Maze{

    static boolean seeker(int[][] array,int x,int y){
        //checks the maze conditions
        if(array[x][y]==0) return false;
        if(x == 4 && y == 4) {
            array[x][y] = 2;
            return true;
        }

        boolean finish = false;
        
        if(x+1 != 5)  finish = finish || seeker(array,x+1,y);
        if(x-1 != -1)  finish = finish || seeker(array,x-1,y);
        if(y+1 != 5)  finish = finish || seeker(array,x,y+1);
        if(y-1 != -1)  finish = finish || seeker(array,x,y-1);

        return finish;
    }



    public static void main(String[] args){

        //Enter the maze
        Scanner input = new Scanner(System.in);
        int[][] array = new int[5][5];

        //Maze created
        for(int i = 0; i < 5; i++){
            for(int j = 0; j < 5; j++){
                array[i][j] = input.nextInt();
            }
        }

        //Calls the function to seek the exit
        System.out.println(seeker(array,0,0));

        //Prints the results
        for(int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}
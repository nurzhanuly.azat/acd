package kz.aitu.week1.Quiz1;

import java.util.Scanner;

public class QuizTask2{

    static int findMax(){
        int someNumber;

        Scanner cin = new Scanner(System.in);
        someNumber = cin.nextInt();
        //condition execute
        if(someNumber == 0) return 0;
        return Math.max(someNumber, findMax());
    }

    public static void main(String[] args){
        System.out.println(findMax());
    }
}

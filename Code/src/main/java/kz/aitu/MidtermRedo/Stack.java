package kz.aitu.MidtermRedo;


public class Stack {
    Node top;

    public void push (int data) {
        Node temp = new Node(data);
        if(top == null) top = temp;
        else {
            top.setNext(temp);
            top = temp;
        }
    }

    public Node pop () {
        if(top == null) return null;
        else {
            Node temp = top;
            top = top.getNext();
            return temp;
        }
    }

    public Node conjugate(Node head1, Node head2, Node head3) {

        Stack superHead = new Stack();
        Node temp1 = head1;
        Node temp2 = head2;
        Node temp3 = head3;

        if( head1 == null && head2 == null){
            while (head3 != null){
                superHead.push(head3.getData());
            }
            return superHead.getTop();
        }
        if( head2 == null && head3 == null){
            while (head1 != null){
                superHead.push(head1.getData());
            }
            return superHead.getTop();
        }
        if( head1 == null && head3 == null){
            while (head2 != null){
                superHead.push(head2.getData());
            }
            return superHead.getTop();
        }


        if(head1 == null) {
            if(head2.getData() > head3.getData()){
                superHead.push(head2.getData());
                head2 = head2.getNext();
                conjugate(null,head2,head3);
            } else {
                superHead.push(head3.getData());
                head3 = head3.getNext();
                conjugate(null,head2,head3);
            }
        }

        if(head2 == null) {
            if(head1.getData() > head3.getData()){
                superHead.push(head1.getData());
                head1 = head1.getNext();
                conjugate(head1,null,head3);
            } else {
                superHead.push(head3.getData());
                head3 = head3.getNext();
                conjugate(head1,null,head3);
            }
        }

        if(head3 == null) {
            if(head2.getData() > head1.getData()){
                superHead.push(head2.getData());
                head2 = head2.getNext();
                conjugate(head1,head2,null);
            } else {
                superHead.push(head3.getData());
                head3 = head3.getNext();
                conjugate(head1,head2,null);
            }
        }

        if(head1 != null && head2 != null && head3 != null){
        while(head1 != null || head2 != null || head3 != null) {
            if (head1 != null) {
                if (head1.getData() > head2.getData() && head1.getData() > head3.getData()) {
                    superHead.push(head1.getData());
                    head1 = head1.getNext();
                    continue;
                }
            }
            if (head2 != null) {
                if (head2.getData() > head1.getData() && head2.getData() > head3.getData()) {
                    superHead.push(head2.getData());
                    head2 = head2.getNext();
                    continue;
                }
            }
            if (head3 != null) {
                if (head3.getData() > head2.getData() && head3.getData() > head1.getData()) {
                    superHead.push(head1.getData());
                    head3 = head3.getNext();
                    continue;
                }
            }
        }
        }
        return superHead.getTop();
    }

    public Node getTop() {
        return top;
    }

    public void setTop(Node top) {
        this.top = top;
    }
}

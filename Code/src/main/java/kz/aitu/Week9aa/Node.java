package kz.aitu.Week9aa;

public class Node {
    private Node next;
    private BinaryNode node;

    public Node(BinaryNode node) {
        this.node = node;
        this.next = null;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public BinaryNode getNode() {
        return node;
    }

    public void setNode(BinaryNode node) {
        this.node = node;
    }
}

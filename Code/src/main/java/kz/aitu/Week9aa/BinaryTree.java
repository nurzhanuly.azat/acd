package kz.aitu.Week9aa;


public class BinaryTree {

    private BinaryNode root;

    public boolean DFS(BinaryNode node, int valueToFind) {
        if (node == null) return false;
        Queue que = new Queue();
        que.add(node);
        while (!(que.isEmpty())) {
            BinaryNode temp = que.poll();
            if (temp.getData() == valueToFind) return true;
            if (temp.getLeft() != null) que.add(temp.getLeft());
            if (temp.getRight() != null) que.add(temp.getRight());
        }
        return false;
    }

    public boolean BFS(BinaryNode node, int valueToFind) {
        if (node == null) return false;
        Stack que = new Stack();
        que.push(node);
        while (!(que.isEmpty())) {
            BinaryNode temp = que.poll();
            if (temp.getData() == valueToFind) return true;
            if (temp.getLeft() != null) que.push(temp.getLeft());
            if (temp.getRight() != null) que.push(temp.getRight());
        }
        return false;
    }

    public void PrintTree(BinaryNode node) {
        if (node == null) return;
        System.out.println(node.getData());
        PrintTree(node.getLeft());
        PrintTree(node.getRight());
    }

    public BinaryNode getRoot() {
        return root;
    }

    public void setRoot(BinaryNode root) {
        this.root = root;
    }

    public BinaryTree() {
        this.root = null;
    }

    public void insert(int data) {
        BinaryNode node = new BinaryNode(data);
        if(root == null) root = node;
        else {
            traverse(root,node);
        }
    }

    private void traverse(BinaryNode node, BinaryNode nodeToAdd) {
        if (node == null) node = nodeToAdd;
        if (node.getData() < nodeToAdd.getData()) {
            if (node.getRight() == null) node.setRight(nodeToAdd);
            else {
                traverse(node.getRight(),nodeToAdd);
            }
        } else {
            if (node.getLeft() == null) node.setLeft(nodeToAdd);
            else {
                traverse(node.getLeft(),nodeToAdd);
            }
        }

    }

}

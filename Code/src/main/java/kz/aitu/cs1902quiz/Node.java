package kz.aitu.cs1902quiz;

public class Node {
    private int data;
    private boolean sameLevel = false;
    private Node left;
    private Node right;
    private Node next;

    public boolean isSameLevel() {
        return sameLevel;
    }

    public void setSameLevel(boolean sameLevel) {
        this.sameLevel = sameLevel;
    }

    public Node(int data) {
        this.data = data;
        this.next = null;
        this.left = null;
        this.right = null;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }
}

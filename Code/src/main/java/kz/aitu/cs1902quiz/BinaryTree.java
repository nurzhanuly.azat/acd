package kz.aitu.cs1902quiz;



public class BinaryTree {
    private Node root;

    int sum = 0;


    public void print(Node node) {
        if (node == null) return;

        print(node.getLeft());
        System.out.println(node.getData());
        print(node.getRight());
    }

    public int TotalSum(Node node) {
        if (node == null) return 0;

        TotalSum(node.getLeft());
        sum += node.getData();
        TotalSum(node.getRight());
        return sum;
    }

    public int getHeight(Node node) {
        int x = goDeep(node.getLeft(),0);
        int y = goDeep(node.getRight(),0);
        return Math.max(x, y);
    }

    private int goDeep(Node node, int c) {
        if (node == null) {
            return 0;
        }
        Node temp = node;
        if (!(node.isSameLevel())) {
            node.setSameLevel(true);
            c++;
            if (node.getRight() != null) node.getRight().setSameLevel(true);

            if (node.getLeft() != null) node.getLeft().setSameLevel(true);

        }
        c += goDeep(node.getLeft(),c);
        c += goDeep(node.getRight(),c);
        return c;
    }

    public int lastOccurence(int[] array, int valueToFind) {
      return binarySearch(array,0,array.length, valueToFind );
    }

    public int binarySearch(int[] array, int left, int right,int valueToFind) {
        if (left == right) return left;
        int middle = left+right/2;
        int index;
        if (array[middle] >= valueToFind) {
            index = binarySearch(array, middle, right, valueToFind);
        } else {
            index = binarySearch(array,left,middle,valueToFind);
        }
        return index;
    }


    public boolean isAnagram(String s1, String s2) {
        int j = s2.length()-1;
        for (int i = 0; i < s1.length(); i++) {
            if ( !(s1.charAt(i) == s2.charAt(j-i))) return false;
        }
        return true;
    }

//    public boolean isLogAnagram(String s1, String s2) {
//        String x = sort(s1);
//        String y = sort(s2);
//        return false;
//    }

//    private String sort(String s1) {
//        String x = QuickSort(s1,0,s1.length()-1);
//        return x;
//    }

//    private String QuickSort(String s, int left, int right) {
//        if (left < right) {
//            int index = partition(s, left, right);
//
//            QuickSort(s, 0, index -1);
//            QuickSort(s, index + 1, right);
//        }
//    }
//
//    private int partition(String s, int left, int right) {
//        char index = s.charAt(right);
//        int j = left - 1;
//
//        for (int i = left; i < right; i++) {
//            if (s.charAt(i) < index) {
//
//            }
//        }
//    }


    public int findMax(Node root) {
        Node temp = root;
        if (root.getRight() != null) {
            temp = temp.getRight();
        while (temp != null){
            if (temp.getRight() != null) temp = temp.getRight();
            if (temp.getLeft() != null) temp = temp.getLeft();
        }
            assert false;
            return temp.getData();
        } else if (temp.getLeft() != null) {
            temp = temp.getLeft();
            while (temp != null) {
                temp = temp.getRight();
            }
            assert false;
            return temp.getData();
        } else {
            return root.getData();
        }
    }

    public void addNode(int data) {
        Node node = new Node(data);
        if(root == null) root = node;
        else {
            traverse(root,node);
        }
    }
    public void traverse (Node root, Node node) {
        if(root == null) {
            root = node;
            return;
        }
        if(root.getData() < node.getData()){
            if(root.getRight() == null) root.setRight(node);
            else
                traverse(root.getRight(),node);
        }
        if(root.getData() > node.getData()){
            if(root.getLeft()==null) root.setLeft(node);
            else
                traverse(root.getLeft(),node);
        }
    }


    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }
}

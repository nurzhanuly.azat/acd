package kz.aitu.Week8;

public class Insertion {
    private int[] array1;

    public void sort(int[] someArray) {
        for(int i = 1; i < someArray.length; i++) {
            int j = i;
            while (someArray[j] < someArray[j-1]) {
                    int temp = someArray[j-1];
                    someArray[j-1] = someArray[j];
                    someArray[j] = temp;
                    if (j == 1) {
                        continue;
                    }
                    j--;
            }
        }
    }
}

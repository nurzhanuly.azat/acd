package kz.aitu.Week8;

import sun.awt.geom.AreaOp;

public class Merge {
    private int[] someArray;

    public void Conquer(int[] left, int[] right) {
        int[] merged = new int[left.length + right.length];
        int merger_length = 0;
        int left_length = 0;
        int right_length = 0;

        while (left_length < left.length && right_length < right.length) {
            if (left[left_length] < right[right_length]){
                merged[merger_length++] = left[left_length++];
            } else {
                merged[merger_length++] = right[right_length++];
            }
        }

        while (left_length < left.length) {
            merged[merger_length++] = left[left_length++];
        }
        while (right_length < right.length) {
            merged[merger_length++] = right[right_length++];
        }
    }

    public void sort(int[] someArray) {
        MergeSort(someArray,someArray.length);
    }

    private void MergeSort(int[] someArray, int n) {
        if (someArray.length <= 1) {
            return;
        }

        int middle = someArray.length/2;
        int[] left = new int[middle];
        int[] right = new int[someArray.length - middle];
        //Filling the containers of new arrays
        for (int i = 0; i < middle; i++) {
            left[i] = someArray[i];
        }
        int j = 0;
        for (int i = middle; i < someArray.length; i++) {
            right[j] = someArray[i];
            j++;
        }

        MergeSort(left, left.length);
        MergeSort(right,right.length);

        Conquer(left,right);

    }
}
